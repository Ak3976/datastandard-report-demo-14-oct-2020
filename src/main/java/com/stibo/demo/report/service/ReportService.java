package com.stibo.demo.report.service;

import com.stibo.demo.report.model.Attribute;
import com.stibo.demo.report.model.AttributeLink;
import com.stibo.demo.report.model.Category;
import com.stibo.demo.report.model.Datastandard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ReportService {
    Logger logger = LoggerFactory.getLogger(ReportService.class);

    public Stream<Stream<String>> report(Datastandard datastandard, String categoryId) {
        // TODO: implement
        logger.info("report() -> Service called");
        long startTime = System.currentTimeMillis();
        List<List<String>> finalData = new ArrayList<>();
        reportRecursive(finalData, datastandard, categoryId);
        Collections.reverse(finalData);
        String arr[] = {"Category Name", "Attribute Name", "Description", "Type", "Groups"};
        finalData.add(0, Arrays.asList(arr));
        logger.info("report() -> Time Taken = {} ms", System.currentTimeMillis() - startTime);
        return finalData.stream().map(x -> x.stream());
    }

    private void reportRecursive(List<List<String>> finalData, Datastandard datastandard, String categoryId) {
        Optional<Category> category = Optional.ofNullable(findCategory(datastandard, categoryId));
        if (category.isPresent()) {
            formCategoryData(finalData, datastandard, category.get());
            if (category.get().getParentId() != null) {
                reportRecursive(finalData, datastandard, category.get().getParentId());
            }
        }
    }

    private Category findCategory(Datastandard datastandard, String categoryId) {
        List<Category> categoryList = datastandard.getCategories().parallelStream()
                .filter(category -> category.getId().equalsIgnoreCase(categoryId)).collect(Collectors.toList());
        return !categoryList.isEmpty() ? categoryList.get(0) : null;
    }

    private Attribute findAttribute(Datastandard datastandard, String attributeId) {
        List<Attribute> attributeList = datastandard.getAttributes().parallelStream()
                .filter(attribute -> attribute.getId().equalsIgnoreCase(attributeId)).collect(Collectors.toList());
        return !attributeList.isEmpty() ? attributeList.get(0) : null;
    }

    private void formCategoryData(List<List<String>> finalData, Datastandard datastandard, Category category) {
        List<AttributeLink> attributeLinks = category.getAttributeLinks();
        for (AttributeLink attributeLink : attributeLinks) {
            Optional<Attribute> attribute = Optional.ofNullable(findAttribute(datastandard, attributeLink.getId()));
            if (attribute.isPresent()) {
                finalData.add(formRowData(datastandard, category, attribute.get(), attributeLink.getOptional()));
            }
        }
    }

    private List<String> formRowData(Datastandard datastandard, Category category, Attribute attribute, Boolean optional) {
        List<String> rowData = new ArrayList<>();
        rowData.add(getValue(category.getName()));
        rowData.add(!optional ? getValue(attribute.getName()) + "*" : getValue(attribute.getName()));
        rowData.add(getValue(attribute.getDescription()));
        rowData.add(attribute.getType().getMultiValue() ? getValue(getCompositeType(datastandard, attribute)) + "[]" : getValue(attribute.getType().getId()));
        rowData.add(getGroups(datastandard, attribute.getGroupIds()));
        return rowData;
    }

    private String getGroups(Datastandard datastandard, List<String> groupIds) {
        String groups = datastandard.getAttributeGroups().stream().filter(attGrp -> groupIds.contains(attGrp.getId()))
                .map(attributeGroup -> attributeGroup.getName())
                .collect(Collectors.joining("\n"));
        return groups;
    }

    private String getCompositeType(Datastandard datastandard, Attribute attribute) {
        StringBuilder multiValueType = new StringBuilder(attribute.getType().getId());
        if (!attribute.getAttributeLinks().isEmpty()) {
            multiValueType.append("{\n");
            for (AttributeLink attributeLink : attribute.getAttributeLinks()) {
                Optional<Attribute> linkedAttribute = Optional.of(findAttribute(datastandard, attributeLink.getId()));
                if (linkedAttribute.isPresent()) {
                    multiValueType.append("  ").append(linkedAttribute.get().getName());
                    if (!attributeLink.getOptional()) {
                        multiValueType.append("*");
                    }
                    multiValueType.append(": ").append(linkedAttribute.get().getType().getId()).append("\n");
                }
            }
            multiValueType.append("}");
        }
        return multiValueType.toString();
    }

    private String getValue(String data) {
        return data != null && !data.isEmpty() ? data : "";
    }
}
